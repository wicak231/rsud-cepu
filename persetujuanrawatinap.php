<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Persetujuan Rawat Inap</title>
</head>
<style>
   .containerhead{ 
    margin: 5px;
    border: 1px solid;
    height: 185px;
   }
   .container{
    margin: 5px;
    border: 1px solid;
    padding:4px;
   }
   .log{
    border: 1px solid;
    padding : 3px;
    text-align : center;
    width : 200px;
    display : inline-block;
   }
   .ttl{
   
    text-align : center;
    width :auto;
    display : inline-block;
    
   }
   .tb0{
    border-collapse: collapse;
   }
   .tb1{
    border: 1px solid;

   }
</style>
<body>
    <div class="containerhead">
        <div class="log">
          <table>
            <tr>
                <td><img src="2.jpeg" width="70" alt=""></td>
            </tr>
            <tr>
                <td>
                    <h4>RSUD Dr. R. SOEPRAPTO</h4>
                    <p><b>Jl. Ronggolawe No.50 Cepu</b></p>
                </td>
            </tr>
          </table>
        </div>
        
       <table class="ttl">
        <tr>
            <td width="150px"></td>    
            <td><h1>PERSETUJUAN RAWAT INAP</h1></td>
        </tr>
        <tr>
           <td height=50px""></td>
        </tr>
       </table>
    </div>
    <div class="container">
       Yang bertanda tangan dibawah ini :
       <table>
        <tr>
            <td width="30px"></td>
            <td>Nama</td>
            <td>:</td>
        </tr>
        <tr>
            <td></td>
            <td width="150px">Tanggal Lahir</td>
            <td>:</td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td>:</td>
        </tr>
       </table>
       Dalam hal ini bertindak sebagai diri sendiri / suami / istri / ayah / ibu / wali / penanggungjawab * dari pasien :
       <table>
        <tr>
            <td width="30px"></td>
            <td>Nama</td>
            <td>:</td>
        </tr>
        <tr>
            <td></td>
            <td>No. Rekam Medis </td>
            <td>:</td>
        </tr>
        <tr>
            <td></td>
            <td width="150px">Tanggal Lahir</td>
            <td>:</td>
        </tr>
       </table>
       Dengan ini saya menyatakan :
       <table>
        <tr>
            <td width="30px">1. </td>
            <td>Pasien tersebut adalah pasien dengan kepesertaan :</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <table class="tb0">
                    <tr class="tb1">
                        <td rowspan="2"  class="tb1">Kepesertaan</td>
                        <td colspan="2"  class="tb1">Persyaratan</td>
                        <td rowspan="2"  class="tb1">Keterangan</td>
                    </tr>
                    <tr class="tb1">
                        <td class="tb1">Lengkap</td>
                        <td class="tb1">Menyusul</td>
                     
                    </tr>
                    <tr class="tb1">
                        <td class="tb1">Umum</td>
                        <td class="tb1">0</td>
                        <td class="tb1">0</td>
                        <td class="tb1">.............</td>
                    </tr>
                </table>
            </td>
        </tr>
       </table>
    </div>
    
</body>
</html>