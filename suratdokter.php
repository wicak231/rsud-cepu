<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Surat Dokter</title>
    <style>
        hr.class-2 {
        border-top: 3px double #000000;
    }
    </style>
</head>
<body >
    <div class="p-4">
        <center><table class="  text-center">
            <tr>
                <td><img src="1.jpeg" alt="" width="100px"></td>
                <td >
                    <h5>PEMERINTAH KABUPATEN BLORA</h5>
                    <h2>RSUD Dr. R SOEPRAPTO</h2>
                    <p>Jl. Rongggolawe 50 Telp.(0296)421026 Fax.424373 E-mail:rs.soeprapto.cepu@gmail.com</p>
                </td>
                <td><img src="2.jpeg" alt="" width="100px"></td>
            </tr>
        </table></center> 
        <hr class="class-2" />
        <h3 class="text-center"><u>SURAT KETERANGAN DOKTER</u></h3>
        <p class="text-center">No.:<b>811</b>/026/III/2023</p>
        <p class="">     Yang bertanda tangan di bawah ini : <b><u>dr. Heru Setyono</u></b></p>
        <p>Dokter Rumah Sakit Umum Daerah dr. R. Soeprapto Cepu Kabupaten Blora menerangkan dengan sesungguhnya bahwa yang tersebut dibawah ini :</p>
        <table>
            <tr>
                <td >NAMA</td>
                <td >:</td>
                <td></td>
            </tr>
            <tr>
                <td>UMUR</td>
                <td>:</td>
                <td></td>
            </tr>
            <tr>
                <td width="150px">JENIS KELAMIN</td>
                <td >:</td>
                <td></td>
            </tr>
            <tr>
                <td>PEKERJAAN</td>
                <td>:</td>
                <td></td>
            </tr>
            <tr>
                <td>ALAMAT</td>
                <td>:</td>
                <td></td>
            </tr>
           
        </table>
        <table class="table table-bordered">
            <tr >
                <thead class="text-center">
                <th>No</th>
                <th colspan="3">Jenis Pemeriksaan</th>     
                <th>Normal</th>
                <th>Kelainan</th>
                <th>Keterangan</th>
                </thead>
            </tr>
            <tr>
                <td width="50px" rowspan="3" class="text-center">1</td>
                <td width="100px" rowspan="3">FISIK</td>
                <td >a. Tinggi</td>
                <td >: 167 cm</td>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
                <td width="200px" rowspan="3">
                    TD:110/60 mmHg<br>
                    N: 97x/menit</td>
            </tr>
            <tr>
                <td >b. Berat</td>
                <td >: 60 Kg</td>
            </tr>
            <tr>
                <td >c. Kulit</td>
                <td >: Sawo Matang</td>
            </tr>
            <tr>
                <td width="50px" rowspan="2" class="text-center">2</td>
                <td width="100px" rowspan="2">MATA</td>
                <td >a. Visus</td>
                <td >: </td>
                <td ></td>
                <td ></td>
                <td width="200px" rowspan="2">
                    VOS : 6/6 <br>
                    VOD: 6/6 <br>
                    <b>TIDAK BUTA WARNA</b>
                </td>
            </tr>
            <tr>
                <td >b. Buta Warna</td>
                <td >: </td>
                <td ></td>
                <td ></td>
            </tr>
            <tr>
                <td width="50px" rowspan="2" class="text-center">3</td>
                <td width="100px" rowspan="2">TELINGA</td>
                <td >a. Kanan</td>
                <td >: </td>
                <td ></td>
                <td ></td>
                <td width="200px" rowspan="2"></td>
            </tr>
            <tr>
                <td >b. Kiri</td>
                <td >: </td>
                <td ></td>
                <td ></td>
            </tr>
            <tr>
                <td class="text-center">4</td>
                <td colspan="3">HIDUNG</td>     
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="text-center">5</td>
                <td colspan="3">LIDAH</td>     
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="text-center">6</td>
                <td colspan="3">PHARYNX</td>     
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="text-center">7</td>
                <td colspan="3">TONSIL</td>     
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="text-center">8</td>
                <td colspan="3">GIGI</td>     
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="text-center">9</td>
                <td colspan="3">THYROID</td>     
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="text-center">10</td>
                <td colspan="3">JANTUNG</td>     
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="text-center">11</td>
                <td colspan="3">PARU-PARU</td>     
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="text-center">12</td>
                <td colspan="3">ABDOMEN</td>     
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td width="50px" rowspan="3" class="text-center">13</td>
                <td width="100px" rowspan="3">REFLEK</td>
                <td >a. Pupil</td>
                <td >: </td>
                <td rowspan="3"></td>
                <td rowspan="3"></td>
                <td width="200px" rowspan="3"></td>
            </tr>
            <tr>
                <td >b. Patella</td>
                <td >: </td>
            </tr>
            <tr>
                <td >c. Achiles</td>
                <td >: </td>
            </tr>
            <tr>
                <td class="text-center">14</td>
                <td colspan="3">THORAX</td>     
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="text-center">15</td>
                <td colspan="3">TIDAK CACAT JASMANI / CACAT JASMANI</td>     
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <p>Menurut pemeriksaan pada hari ini Kesehatan Badannya Baik / Tidak Baik untuk :</p>
        <p>PERSYARATAN MASUK KERJA</p>
    </div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> 
</body>
</html>