<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gneeral Concent</title>
</head>
<style>
    body{
        padding: 2px;
    }
    .line{
        border: 1px solid;
    }
    .no{
        border: 1px solid;
        text-align: center;
    }
    .tb1{
        border-collapse: collapse;
        border: 1px solid;
        width: 100%;
      
    }
</style>
<body>
<div style="text-align: right;">
<b>RM.1.3</b>
</div>
<table class="tb1">
<tr>
<td width="130px">
<table style=" text-align: center;  ">
<tr><td><img src="2.jpeg" width="100" alt=""></td></tr>
<tr><td style=" font-size:20px "><b>RSUD Dr. R Soeprapto</b></td></tr>
                    <tr><td style=" font-size:15px "><b>Jl. Ronggolawe No.50</b></td></tr>
                </table>
            </td>
            <td class="line" width="200">
                <table>
                    <tr>
                        <td>NO. RM</td>
                        <td>:</td>
                    </tr>
                    <tr>
                        <td width="100px">NAMA PASIEN</td>
                        <td>:</td>
                    </tr>
                    <tr>
                        <td>TGL. LAHIR</td>
                        <td>:</td>
                    </tr>
                    <tr>
                        <td>ALAMAT</td>
                        <td>:</td>
                    </tr>
                </table>
            </td>
            <td width="80">
                <table style=" text-align: center; ">
                    <tr><td style=" font-size:20px "><b>PERSETUJUAN UMUM / </b></td></tr>
                    <tr><td style=" font-size:20px "><b>GENERAL CONCENT</b></td></tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="tb1" style="font-size:10px;">
        <tr>
            <th class="no" width="30px">No</th>
            <th class="line"width="150">MATERI PENJELASAN</th>
            <th class="line">ISI MATERI</th>
        </tr>
        <tr>
            <td class="no">1</td>
            <td class="line">HAK PASIEN DAN KELUARGA</td>
            <td class="line">
                <img src="4.png" width="12"> Saya mengerti dan memahami tentang <b>HAK PASIEN DAN KELUARGA</b> sesuai Undang-undang Kesehatan No.44 tahun 2009 tentang Rumah Sakit <br> 
                <img src="4.png" width="12"> Pasien / keluarga memiliki hak untuk mengajukan pertanyaan tentang pengobatan yang diusulkan setiap saat. <br>
                <img src="4.png" width="12"> Saya mengerti dan memahami bahwa memiliki hak untuk menyetujui / menolak setiap prosedur/terapi. <br>
                <img src="4.png" width="12"> Jika diperlukan Rumah Sakit, saya akan berpartisipasi dalam pemilihan Dokter Penanggung Jawab Pelayanan selama perawatan saya di Rumah Sakit
            </td>
        </tr>
        <tr>
            <td class="no">2</td>
            <td class="line">PELAYANAN KEROHANIAN</td>
            <td class="line">
                <img src="4.png" width="12"> Saya memahami informasi tentang pelayanan kerohanian di Rumah Sakit sesuai agama/kepercayaan pasien, dan cara bimbingan kerohanian sesuai fasilitas Rumah Sakit yang ada/keinginan pasien/keluarga
            </td>
        </tr>
        <tr>
            <td class="no">3</td>
            <td class="line">KEBUTUHAN PRIVASI, NILAI-NILAI, DAN KEYAKINAN PASIEN</td>
            <td class="line">
                <img src="4.png" width="12"> Saya <b>mengijinkan / tidak mengijinkan</b> (coret salah satu) Rumah Sakit memberi akses bagi keluarga dan handai taulan serta orang yang akan menjenguknya (sebutkan nama/profesi jika ada permintaan)
                ............................................ <br>
                <img src="4.png" width="12"> Nilai-nilai dan keyakinan pasien, jika ada (coret salah satu) dibawah ini : <br>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <img src="3.png" width="8"> Menolak dilakukan tranfusi darah <br>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <img src="3.png" width="8"> Menolak pulang pada hari tertentu. Hari .......................... <br>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <img src="3.png" width="8"> Menolak dilayani oleh petugas laki-laki pada pasien perempuan atau sebaliknya <br>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <img src="3.png" width="8"> Menolak memakan makanan suatu jenis makanan tertentu. Makanan ........................ <br>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <img src="3.png" width="8"> Menolak obat-obatab yang mengandung bahan-bahan tertentu <br>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <img src="3.png" width="8"> Lain-lain ............................................


            </td>
        </tr>
        <tr>
            <td class="no">4</td>
            <td class="line">KEAMANAN</td>
            <td class="line">
                <img src="4.png" width="12"> Saya <b>menyetujui</b> peraturan Rumah Sakit untuk melarang keluarga dan atau handai taulan menjenguk di luar jam besuk. <br>
                <img src="4.png" width="12"> Keluarga yang berkunjung di luar jam besuk WAJIB membawa kartu identitas pengunjung yang telah ditetapkan Rumah Sakit demi keselamatan dan kesembuhan pasien.
            </td>
        </tr>
        <tr>
            <td class="no">5</td>
            <td class="line">TATA TERTIB & PEMELIHARAAN FASILITAS RS</td>
            <td class="line">
                <img src="4.png" width="12"> Saya mengerti dan memahami jika terjadi kerusakan atau kehilangan yang disebabkan oleh pasien maka menjadi tanggung jawab pasien termasuk fasilitas umum dan fasilitas/alat medis. <br>
                <img src="4.png" width="12">  Saya bersedia mematuhi tata tertib yang ada di RSUD dr. R. Soeprapto Cepu.
            </td>
        </tr>
        <tr>
            <td class="no">6</td>
            <td class="line">PERSETUJUAN PELAYANAN KESEHATAN</td>
            <td class="line">
                <img src="4.png" width="12"> Saya menyetujui dan memberikan persetujuan untuk mendapat pelayanan kesehatan di RSUD dr. R. Soeprapto Cepu dan dengan ini saya meminta dan memberikan kuasa kepada RSUD dr. R.Soeprapto Cepu, dokter dan perawat dan tenaga kesehatan lainnya untuk memberikan asuhan perawatan, pemeriksaan fisik yang dilakukan oleh dokter dan perawat dan melakukan prosedur diagnostik, radiologi dan/atau terapi dan tatalaksana sesuai pertimbangan dokter yang diperlukan atau disarankan pada perawatan saya. Hal ini mencakup seluruh pemeriksaan, prosedur rutin, termasuk EKG, X-Ray, Test Darah, terapi fisik, pemberian obat, dan pemasangan alat kesehatan (DC, NGT, Pemasangan Infus)
            </td>
        </tr>
        <tr>
            <td class="no">7</td>
            <td class="line">PELEPASAN INFORMASI</td>
            <td class="line">
                <img src="4.png" width="12"> Memahami informasi yang ada dalam diri saya, termasuk diagnosis, hasil laboratorium hasil tes diagnostik yang akan digunakan untuk perawatan medis, Rumah Sakit akan menjamin kerahasiaannya. <br>
                <img src="4.png" width="12"> Memberi wewenang Rumah Sakit untuk memberikan informasi tentang diagnosis, hasil pelayanan dan pengobatan bila diperlukan tidak terbatas untuk memproses klaim asuransi dan atau lembaga pemerintah. <br>
                <img src="4.png" width="12"> Memberikan wewenang kepada RS untuk memberikan kewenangan untuk terlibat dalam pengambilan keputusan mengenai perawatan saya data dan informasi mengenai diri saya dan keadaan kesehatan saya termasuk dalam situasi tertentu misalnya keadaan kritis dll, kepada keluarga saya, bernama : <br>
                1. &nbsp .................... &nbsp&nbsp 3. &nbsp ...................... <br>
                2. &nbsp .................... &nbsp&nbsp 4. &nbsp ...................... <br>
            </td>
        </tr>
        <tr>
            <td class="no">8</td>
            <td class="line">BARANG BERHARGA DAN MILIK PRIBADI</td>
            <td class="line">
                <img src="4.png" width="12"> Memahami bahwa <b>Rumah Sakit tidak bertanggung jawab</b> atas semua kehilangan barang-barang berharga milik saya dan saya secara pribadi bertanggung jawab atas barang-barang berharga milik saya tersebut. Barang milik pasien dilindungi apabila RSUD CEPU mengambil alih tanggung jawab atau pada saat pasien tidak mampu bertanggungjawab atas barang miliknya dan tanpa pendamping atau keluarga. <br>
            </td>
        </tr>
        <tr>
            <td class="no">9</td>
            <td class="line">HASIL PELAYANAN</td>
            <td class="line">
                <img src="4.png" width="12"> Saya menyadari bahwa praktek kedokteran bukanlah ilmu pasti dan mengerti bahwa tidak ada jaminan atas hasil pengobatan atau tindakan yang akan diberikan. Saya akan mengikuti pengobatan medis sesuai prognosis Dokter sesuai dengan standar kedokteran dan saya berharap semoga diberikan yang terbaik oleh Tuhan YME.
            </td>
        </tr>
        <tr>
            <td class="no">10</td>
            <td class="line">TANGGUNG JAWAB PEMBIAYAAN</td>
            <td class="line">
                <img src="4.png" width="12"> Saya memahami tentang informasi biaya pengobatan atau biaya tindakan yang dijelaskan oleh petugas RSUD CEPU<br>
                <img src="4.png" width="12"> Saya mengijinkan Rumah Sakit untuk menagih pembayaran saya (termasuk Asuransi atau penjamin lain) untuk seluruh pelayanan medik, teknis dan fasilitas, lebih lanjut saya mengijinkan Rumah Sakit untuk memberikan informasi dari rekam medis yang diperlukan untuk kepentingan pembayaran. Jika Asuransi atau penjamin tidak menanggung sebagian atau keseluruhan tagihan, saya pribadi akan bertanggung jawab untuk biaya yang tidak ditanggung.<br>
                <img src="4.png" width="12"> Saya mengerti bahwa setiap pertanggungan kesehatan terhadap saya harus sesuai dengan polis Asuransi atau benefit yang saya miliki. Jika saya tidak diasuransikan maka saya setuju untuk bertanggungjawab secara penuh atas pembiayaan yang timbul dari pelayanan kesehatan yang saya terima.<br>
            </td>
        </tr>
        <tr>
            <td class="no">11</td>
            <td class="line">PESERTA DIDIK/PELATIHAN</td>
            <td class="line">
                <img src="4.png" width="12"> Saya mengetahui bahwa RSUD Dr. R Soeprapto Cepu menjadi lahan praktek utuk mahasiswa kedokteran dan profesi lain, maka saya <b>mengijinkan / tidak mengijinkan</b> (coret salah satu) kepada mereka untuk berpartisipasi dalam pengobatan dan perawatan saya baik secara langsung maupun tidak langsung.<br>
            </td>
        </tr>
    </table>
    <table class="tb1" style="font-size:11px;">
        <tr>
            <td class="line" width="630">Dengan ini menyatakan bahwa saya telah menerangkan hal-hal diatas secara benar dan jelas, memberikan kesempatan untuk bertanya dan berdiskusi.</td>
            <td class="line">
                <table style=" text-align: center">
                    <tr><td>Petugas</td></tr>
                    <tr><td height="30"></td></tr>
                    <tr><td>(.............................)</td></tr>
                    <tr><td>Tanda Tangan & nama terang</td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="line">Dengan ini menyatakan saya telah memahami dan sepenuhnya SETUJU dengan setiap pernyataan yang terdapat pada PERSETUJUAN UMUM/GENERAL CONSENT dan saya menandatangani dengan kesadaran penuh dan tanpa paksaan.</td>
            <td class="line">
            <table style=" text-align: center">
                    <tr><td>Pasien/Keluarga</td></tr>
                    <tr><td height="30"></td></tr>
                    <tr><td>(.............................)</td></tr>
                    <tr><td>Tanda Tangan & nama terang</td></tr>
                </table>
            </td>
        </tr>
    </table>

    
</body>
</html>