<!DOCTYPE html>
<head>
    <title>Contoh Surat Pernyataan</title>
    <meta charset="utf-8">
    <style>
        #judul{
            text-align:center;
        }
        #h0{
            width: auto; 
            height:auto; 
            text-align: right;  
            position: center; 
        }

        #h1{
            width: auto; 
            height: 330px; 
            position: center; 
            border: 1px solid; 
            padding:15px;
        }
        #h2{
            width: auto; 
            height: auto; 
            position: center; 
            border: 1px solid; 
            padding:15px;
        }
        #h3{
            width: auto; 
            height: auto; 
            position: center; 
            border: 1px solid; 
            padding: 15px;
        }

        .rm{
            margin-left: 800px;
            margin-bottom: 100px;
        }

        .norm{
            padding-bottom: 1px;
        }

        .table{
            border:     ; 
            text-align: center;
        }
        .f1{
            padding: 20px;
            border: 1px solid;
            text-align: left;  
            font-size : 25px; 
            width: auto; 
        }
        .f2{
            padding: 20px;
            text-align: left;  
            font-size : 25px; 
            width: auto; 
        }
        .f3{
            padding: 10px;
            text-align: left;  
            font-size : 25px; 
            width: auto; 
        }
        .ttd{
        
            text-align: center;
            font-size : 25px; 
            margin-left : 800px;
        }
    </style>
</head>
<body>
    <div id=h0>
        <h2 class="norm"><b>RM.1.1</b></h2>
    </div>
    <div id=h1>
       <table class="table" >
        <tr>
            <td><img src="2.jpeg" width="100"></td>
            <td rowspan="3" width="300"></td>
            <td rowspan="3"  style="font-size:30px;"><b>PENDAFTARAN PASIEN BARU</b></td>
        </tr>
        <tr style="font-size:20px;">
            <td><b>RSUD Dr. R. Soeprapto</b></td>
        </tr>
        <tr>
            <td><b>Jl. Ronggolawe No.50 Cepu</b></td>
        </tr>
       </table><br>
       <table class="f1">
        <tr>
            <td colspan="2">Kartu identitas yang digunakan :</td>
        </tr>
        <tr>
            <td width="200px">KTP</td>
            <td>No. :...............................</td>
        </tr>
        <tr>
            <td >PASPORT</td>
            <td>No. :...............................</td>
        </tr>
        <tr>
            <td></td>
        </tr>
       </table>
    </div>
    <div id=h2>
       <table class="f2">
        <tr>
            <td width="300px">Nama Lengkap Pasien</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Tempat Lahir</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Tanggal Lahir</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td> Alamat</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Propinsi</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Nomor Telepon</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Nama Perusahaan</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>No. Telepon Kantor</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Agama</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Etnis</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Bahasa</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Status</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Pendidikan</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Warga Negara</td>
            <td>:</td>
            <td></td>
        </tr>
       </table>
    </div>
    <div id=h3>
        <table class="f3">
            <tr>
                <td><b>Penanggung Jawab</b></td>
                <td></td>
            </tr>
            <tr>
                <td width="300">Nama</td>
                <td>:</td>
            </tr>
            <tr>
                <td>Hubungan</td>
                <td>:</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
            </tr>
            <tr>
                <td>No. Telepon</td>
                <td>:</td>
            </tr>
            <tr>
                <td>Kartu Identitas</td>
                <td>:</td>
            </tr>
            <tr>
                <td>Nomor Identitas</td>
                <td>:</td>
            </tr>
        </table>
        <table  class="ttd">
            <tr>
                <td>Cepu,.................................</td>
                <td>&nbsp&nbsp&nbsp&nbsp</td>
            </tr>
            <tr>
                <td>Pendaftar,</td>
            </tr>
            <tr>
                <td height="50px"></td>
            </tr>
            <tr>
                <td>(..........................................)</td>
            </tr>
            <tr>
                <td>Tanda tangan & nama terang</td>
            </tr>
        </table>
    </div>
</body>

</html>