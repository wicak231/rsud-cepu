<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        hr.class-2 {
        border-top: 3px double #000000;
    }
    </style>
    <title>Formulir Pemeriksaan Radiologi</title>
</head>
<body class="p-4">
<center><table class="  text-center">
            <tr>
                <td><img src="1.jpeg" alt="" width="100px"></td>
                <td >
                    <h5>PEMERINTAH KABUPATEN BLORA</h5>
                    <h2>RSUD Dr. R SOEPRAPTO</h2>
                    <p>Jl. Rongggolawe 50 Telp.(0296)421026 Fax.424373 E-mail:rs.soeprapto.cepu@gmail.com</p>
                </td>
                <td><img src="2.jpeg" alt="" width="100px"></td>
            </tr>
        </table></center> 
        <hr class="class-2" />
    <h3 class="text-center">FORMULIR PEMERIKSAAN RADIOLOGI</h3>
    <p class="text-center">CALON PEGAWAI NEGERI SIPIL, PEWGAWAI NEGERI SIPIL DAN TENAGA KERJA LAINNYA</p>
    <table class="table table-bordered">
        <tr>
            <td colspan="3"> <b>Bagian A - Identitas dan Surat Pernyataan Pasien <i>(diisi dengan lengkap oleh Karyawan)</i></b></td>
        </tr>
        <tr>
            <td>Nama Lengkap :</td>
            <td>KTP/KK/Paspor:</td>
            <td>
                Jenis Kelamin : <br>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                        Laki-laki
                    </label>
                 </div>
                 <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                        Perempuan
                    </label>
                 </div>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> 
</body>
</html>