<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>General Concent</title>
</head>
<style>
     body{
        padding: 2px;
        }
    .tb1{
        border-collapse: collapse;
        border: 1px solid;
        width: 100%;
        text-align: center;
        font-size: 22px;
        }
    .tb2{
        border-collapse: collapse;
        border: 1px solid;
        width: 100%;
        padding: 10px;
        font-size: 14px;
        }
    }
       
    .line{
        border: 1px solid;
        }
</style>
<body>
<div style="text-align: right;">
        <b>RM.1.3a</b>
    </div>
    <table class="tb1">
        <tr>
            <td "><img src="1.jpeg" width="100"></td>
            <td>
                PEMERINTAH KABUPATEN BLORA <br>
                <b>RUMAH SAKIT UMUM DAERAH</b><br>
                <b>dr. R. SOEPRAPTO</b><br>
                Jl. Ronggolawe 50 Cepu Telp.(0296)421026, Fax: 424373 <br>
                E-Mail:brsdcepu@plasa.com <br>
                CEPU - 58311
            </td>
            <td><img src="2.jpeg" width="100"></td>
        </tr>
    </table>
    <table class="tb2" >
        <tr>
            <td>
                    <table >
                    <tr>
                        <td style="padding: 10px;">
                            <b>HAK PASIEN :</b> <br>
                            1. Pasien berhak memperoleh informasi mengenai tata tertib dan peraturan yang berlaku di rumah sakit. <br>
                            2. Pasien berhak memperoleh informasi tentang hak dan kewajiban pasien.<br>
                            3. Pasien berhak memperoleli layanan yang manusiawi, adil, jujur, dan tanpa diskriminasi.<br>
                            4. Pasien berhak memperoleh layanan kesehatan yang bermutu sesuai dengan standar profesi dan standar prosedur <br> 
                            &nbsp&nbsp&nbsp operasional.<br>
                            5. Pasien berhak memperoleh layanan yang efektif dan efisien sehingga pasien terhindar dari kerugian fisik dan <br> 
                            &nbsp&nbsp&nbsp materi. <br>
                            6. Pasien berhak mengajukan pengaduan atas kualitas pelayanan yang di dapatkan.<br>
                            7. Pasien berhak memilih dokter dan kelas perawatan sesuai dengan keinginannya dan sesuai dengan peratran yang <br> 
                            &nbsp&nbsp&nbsp berlaku di Rumah Sakit.<br>
                            8. Pasien berhak meminta konsultasi tentang penyakit yang di deritanya kepada dokter lain yang mempunyai Surat <br> 
                            &nbsp&nbsp&nbsp Ijin Praktek (SIP) baik di dalam atau di luar rumah sakit.<br>
                            9. Pasien berhak mendapatkan privasi dan kerahasiaan penyakit yang di derita termasuk data-data medisnya.<br>
                            10.Pasien berhak mendapatkan informasi yang meliputi diagnosis dan tata cara tindakan medis, tujuan tindakan <br> 
                            &nbsp&nbsp&nbsp medis, alternative tindakan, resiko dan komplikasi yang mungkin terjadi dan prognosis terhadap tindakan yang <br> 
                            &nbsp&nbsp&nbsp dilakukan serta perkiraan biaya pengobatan.<br>
                            11.Pasien berhak memberikan persetujuan atau menolak atas tindakan yang akan di lakukan oleh tenaga kesehatann <br> 
                            &nbsp&nbsp&nbsp terhadap penyakit yang di deritanya.<br>
                            12.Pasien berhak di dampingi keluarganya dalam keadaan kritis.<br>
                            13.Pasien berhak menjalankan ibadah sesuai agama atau kepercayaan yang di anutnya selama hal itu tidak <br> 
                            &nbsp&nbsp&nbsp mengganggu pasien lainnya.<br>
                            14.Pasien berhak memperoleh keamanan dan keselamatan dirinya selama dalam perawatan di rumah sakit.<br>
                            15.Pasien berhak mengajukan usul, saran, perbaikan atas perlakuan rumah sakit terhadap dirinya.<br>
                            16.Pasien berhak menolak pelayanan bimbingan rohani yang tidak sesuai dengan agama dan kepercayaan yang di <br> 
                            &nbsp&nbsp&nbsp anutnya.<br>
                            17.Pasien berhak menggugat dan/atau menuntut rumah sakit apabila rumah sakit di duga memberikan pelayanan <br> 
                            &nbsp&nbsp&nbsp yang tidak sesuai dengan standar baik secara perdata maupun pidana.<br>
                            18.Pasien berhak mengeluhkan pelayanan rumah sakit yang tidak sesuai dengan standar pelayanan melalui media <br> 
                            &nbsp&nbsp&nbsp cetak dan elektronik sesuai dengan peraturan perundang-undangan. <br><br>

                            <b>KEWAJIBAN PASIEN :</b> <br>
                            1. Mematuhi peraturan yang berlaku di rumah sakit <br>
                            2. Menggunakan fasilitas rumah sakit secara bertanggung jawab <br>
                            3. Menghormati hak-hak pasien lain, pengunjung dan hak tenaga kesehatan srta petugas lainnya yang bekerja di rumah sakit. <br> 
                            4. Memberikan informasi yang jujur,, lengkap dan akurat sesuai kemampuan dan pengetahuannya tentang masalah kesehatannya. <br>
                            5. Memberikan informasi mengenai kemampuan financial dan jaminan kesehatan yang dimilikinya. <br>
                            6. Mematuhi rencana terapi yang direkomendasikan oleh tenaga kesehatan di rumah sakit dan disetujui oleh pasien yang <br> 
                            &nbsp&nbsp&nbsp bersangkutan setelah mendapatkan penjelasan sesuai ketentuan perturan perundang undangan. <br>
                            7. Menerima segala konsekuensi atau keputusan pribadinya untuk menolak rencana terapi yang direkomendasikan oleh tenaga <br> 
                            &nbsp&nbsp&nbsp kesehatan dan/atau tidak mematuhi petunjuk yang diberikan oleh tenaga kesehatan dalam rangka penyembuhan penyakit atau <br> 
                            &nbsp&nbsp&nbsp masalah kesehatannya. <br>
                            8. Memberikan imbalan jasa atau pelayanan yang diterima. Pasien/Keluarga pasien
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td height="20px">&nbsp</td></tr>
        <tr>
            <td>
                <br><table style="text-align:center;">
                <tr>
                    <td width="140">&nbsp</td>
                    <td>Petugas</td>
                    <td width="100">&nbsp</td>
                    <td>Pasien/Keluarga Pasien</td>
                </tr>
                <tr><td height="40"></td></tr>
                <tr>
                <td></td>
                    <td>(...............................................)</td>
                    <td></td>
                    <td>(...............................................)</td>
                    <td></td>
                </tr>
                <tr><td height="40px">&nbsp</td></tr>
                </table>
            </td>
        </tr>
    </table>   
</body>
</html>